import axios from "axios"
import { BASE_URL } from "../../api"
import { actionTypes } from "./actionType"

export const fetchProfile = (token) => {
    return(dispatch) => {
        axios(`${BASE_URL}auth/profile`, {
            headers: {
                "Authorization": `bearer ${token}`
            }
        })
        .then(resp => dispatch({
            type: actionTypes.GET_PROFILE,
            payload: resp.data
        }))
        .catch(er => {
            console.log("error in get profile", er)
        })
    }
}