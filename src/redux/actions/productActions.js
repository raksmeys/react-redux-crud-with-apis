import axios from "axios"
import { BASE_URL } from "../../api"
import { actionTypes } from "./actionType"

// create function to get products
export const getAllProducts = () => {
    return (dispatch) => {
        axios(`${BASE_URL}products`)
        .then(res => dispatch({
            type: actionTypes.GET_PRODUCTS,
            payload: res.data
        }))
        .catch(er => {
            console.log('issue in fetch product', er)
        })
    }
}

// create function to get products
export const searchProducts = (title) => {
    return (dispatch) => {
        axios(`${BASE_URL}products?title=${title}`)
        .then(res => dispatch({
            type: actionTypes.SEARCH_PRODUCT,
            payload: res.data
        }))
        .catch(er => {
            console.log('issue in search products', er)
        })
    }
}

export const deleteProduct = (id) => {
    return (dispatch) => {
        axios(`${BASE_URL}products/${id}`, { method: "DELETE"})
        .then(res => dispatch({
            type: actionTypes.DELETE_PRODUCT,
            payload: {id}
        }))
    }
}

// update product by id
export const updateProduct = (id, product) => {
    return (dispatch) =>{
        axios(`${BASE_URL}products/${id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(product)
        })
        .then(response => dispatch({
            type: actionTypes.UPDATE_PRODUCT,
            payload: response.data
        }))
    }
}

export const createProduct = (product) => {
    return (dispatch) =>{
        axios(`${BASE_URL}products`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(product)
        })
        .then(response => dispatch({
            type: actionTypes.CREATE_PRODUCT,
            payload: response.data
        }))
    }
}