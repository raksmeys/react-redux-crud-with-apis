// reducer is function which check on action and return state
import { actionTypes } from "../actions/actionType"
const initialState = {
    products: [],
    isLoading: true
}

export const productReducer = (state = initialState, action) => {
    let {GET_PRODUCTS, SEARCH_PRODUCT, DELETE_PRODUCT, UPDATE_PRODUCT, CREATE_PRODUCT} = actionTypes
    let {type, payload} = action
    switch(type){
        case GET_PRODUCTS:
            return {...state, products: payload, isLoading: false}
        case SEARCH_PRODUCT:
            return {...state, products: payload, isLoading: false}
        case CREATE_PRODUCT: 
            return{...state, products: state.products.push(payload)}
        case DELETE_PRODUCT:
            return {products: state.products.filter(({ id }) => id !== payload.id)};
        case UPDATE_PRODUCT: 
            return {
                ...state,
                products: state.products.map(product => product.id === payload.id ? payload : product)
            }
        default:
            return state
    }
}

