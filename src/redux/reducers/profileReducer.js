// reducer is function which check on action and return state
import { actionTypes } from "../actions/actionType"
const initialState = {
    profile: {
        "avatar": "https://flowbite.s3.amazonaws.com/blocks/marketing-ui/avatars/michael-gough.png"
    }
}

export const profileReducer = (state = initialState, action) => {
    let {GET_PROFILE} = actionTypes
    let {type, payload} = action
    switch(type){
        case GET_PROFILE:
            return {...state, profile: payload}
        default:
            return state
    }
}