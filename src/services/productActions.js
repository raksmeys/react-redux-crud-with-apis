import axios from "axios"

const BASE_URL = 'https://api.escuelajs.co/api/v1/'
/// MARK: function to get all products data
export const fetchProducts = async (limit) => {
    const response = await fetch(`${BASE_URL}products?offset=0&limit=${limit}`)
    return response.json()
}

/// MARK: function to get product by id
export const fetchProduct = async (id) => {
    const response = await fetch(`${BASE_URL}products/${id}`)
    return response.json()
}


/// Mark: Search Product by title
export const searchProducts = async (title) => {
    const response = await fetch(`${BASE_URL}products?title=${title}`)
    return response.json()
}

/// MARK: function to get categories
export const fetchCategories = async () => {
    const response = await fetch(`${BASE_URL}categories`)
    return response.json()
}

/// MARK: function to insert product 
export const insertProduct = async (product) => {
    const response = await axios(`${BASE_URL}products`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        }, 
        data: JSON.stringify(product)
    })
    return response
}

/// function to upload image to server
export const uploadImageToServer = async (image) => {
    const response = await axios(`${BASE_URL}files/upload`, {
        method: "POST",
        headers: {
            "Content-Type": "multipart/form-data"
        },
        data: image
    })
    return response
}