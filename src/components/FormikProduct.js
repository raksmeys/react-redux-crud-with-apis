import React, { useEffect, useState } from "react";
import { fetchCategories, uploadImageToServer } from "../services/productActions";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { createProduct, updateProduct } from "../redux/actions/productActions";
import { useLocation } from "react-router-dom";

export default function ProductFormValidation({isEditing}) {

  const dispatch = useDispatch()
  const location = useLocation() // for get data from navigation

  const [categories, setCategories] = useState([]);
  const [source, setSource] = useState("")

// initialize formik validation
const formik = useFormik({
    initialValues: {
        id: 0,
        title: "",
        price: 0,
        description: "",
        categoryId: 0,
        images: ""
    },
    validationSchema: yup.object({
        title: yup.string().min(15).required("Please enter product title"),
        price: yup.number().positive().required("Please enter price of products"),
        description: yup.string().required("Please enter product description"),
        categoryId: yup.string().required("Please choose category"),
        images: yup.mixed()
                   .required("Please choose image")
                   .test('FILE_SIZE', "Too Big", (value) => value && value.size < 1024*1024)
                   .test("FILE_TYPE", "Invalid", 
                    (value) => value && ['image/png', 'image/jpg', 'image/jpeg'].includes(value.type))
    }),
    onSubmit: (values) => {
        console.log(values)
        console.log('is edit? ', isEditing)
        if(isEditing){
           // is user pick new image for product?
           if (source == ""){
              // will execute when user don't choose new image
              dispatch(updateProduct(formik.values.id, values))
              console.log('id before update', formik.values.id)
           }else{
             // will execute when user browse new images for product
              let image = new FormData()
              image.append('file', source)
              uploadImageToServer(image)
              .then(response => {
                console.log(response.data.location)
                formik.values.images = [response.data.location]
                console.log(values)
                console.log('id befor update', formik.values.id)
                dispatch(updateProduct(formik.values.id, values))
              })
              .catch(er => {
                console.log(er)
              })
           }
        }else{
          // block code execute when user insert product
           // console.log(product)
           let image = new FormData()
           image.append('file', source)
           // perform upload image first
           uploadImageToServer(image)
           .then(response => {
              // assign url image to state
              formik.values.images = [response.data.location]
              // final insert product with image
              dispatch(createProduct(values))
            })
        } 
    }
})

/// destructuring object
  const {title, price, description, categoryId, images} = formik.values 

  useEffect(() => {
    fetchCategories().then((res) => setCategories(res));

    // checking data pass from navigation
    if(isEditing){
      let {id, title, price, description, category, images} = location.state
      formik.values.id = id
      formik.values.title = title
      formik.values.description = description
      formik.values.price = price
      formik.values.images = images
      formik.values.categoryId = category.id
      console.log("title", title)
      console.log(formik.values)
    }
    
  }, []);

  return (
    <main className="mt-10">
      <section className="container mx-auto w-1/2">
        <form
            onSubmit={formik.handleSubmit} 
            className="mt-10">
          <div class="grid gap-4 mb-4 sm:grid-cols-2">
            <div>
              <label
                for="title"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Title
              </label>
              <input
                type="text"
                name="title"
                id="title"
                value={title}
                onChange={formik.handleChange}
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                placeholder="Ex. Apple iMac 27&ldquo;"
              />
              {
                formik.errors && <p className="text-red-900">{formik.errors.title}</p>
              }
            </div>
            <div>
              <label
                for="brand"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Brand
              </label>
              <input
                type="text"
                name="brand"
                id="brand"
                value="Google"
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                placeholder="Ex. Apple"
              />
            </div>
            <div>
              <label
                for="price"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Price
              </label>
              <input
                type="number"
                name="price"
                id="price"
                value={price}
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                placeholder="$299"
                onChange={formik.handleChange}
              />
              {
                formik.errors && <p className="text-red-900">{formik.errors.price}</p>
              }
            </div>
            <div>
              <label
                for="category"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Category
              </label>
              <select
                id="categoryId"
                name="categoryId"
                onChange={formik.handleChange}
                value={categoryId}
                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
              >
                {/* <option>Product Category</option> */}
                {categories.map((category, index) => (
                  <option 
                    key={index}
                    value={category.id}>{category.name}</option>
                ))}
              </select>
            </div>
            <div class="sm:col-span-2">
              <label
                for="description"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Description
              </label>
              <textarea
                id="description"
                rows="5"
                value={description}
                onChange={formik.handleChange}
                name="description"
                class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-primary-500 focus:border-primary-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                placeholder="Write a description..."
              ></textarea>
              {
                formik.errors && <p className="text-red-900">{formik.errors.description}</p>
              }
            </div>

            <div class="flex items-center justify-center w-full">
              <label
                for="dropzone-file"
                class="flex flex-col items-center justify-center w-full h-64 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600"
              >
                <div class="flex flex-col items-center justify-center pt-5 pb-6">
                  <svg
                    class="w-8 h-8 mb-4 text-gray-500 dark:text-gray-400"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 20 16"
                  >
                    <path
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M13 13h3a3 3 0 0 0 0-6h-.025A5.56 5.56 0 0 0 16 6.5 5.5 5.5 0 0 0 5.207 5.021C5.137 5.017 5.071 5 5 5a4 4 0 0 0 0 8h2.167M10 15V6m0 0L8 8m2-2 2 2"
                    />
                  </svg>
                  <p class="mb-2 text-sm text-gray-500 dark:text-gray-400">
                    <span class="font-semibold">Click to upload</span> or drag
                    and drop
                  </p>
                  <p class="text-xs text-gray-500 dark:text-gray-400">
                    SVG, PNG, JPG or GIF (MAX. 800x400px)
                  </p>
                </div>
                <input
                  id="dropzone-file"
                  type="file"
                  onChange={(e) => {
                      // validate images whether it pass the test
                      formik.setFieldValue("images", e.target.files[0])
                      // setSource for preview
                     setSource(e.target.files[0])
                  }}
                  class="hidden"
                />
              </label>
            </div>
            <div>
              <img 
                  src={source == "" ? formik.values.images[0] : URL.createObjectURL(source)}
                  width={300}
                />
                {
                formik.errors && <p className="text-red-600">{formik.errors.images}</p>
                }
            </div>
          </div>
          <div class="flex justify-end space-x-4 ">
            <button
              type="submit"
              class="text-white bg-primary-700 hover:bg-primary-800 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800"
            >
              {
                isEditing ? "Update Product" : "Create Product"
              }
            </button>
            <button
              type="button"
              class="text-red-600 inline-flex items-center hover:text-white border border-red-600 hover:bg-red-600 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:border-red-500 dark:text-red-500 dark:hover:text-white dark:hover:bg-red-600 dark:focus:ring-red-900"
            >
              <svg
                class="mr-1 -ml-1 w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                  clip-rule="evenodd"
                ></path>
              </svg>
              Cancel
            </button>
          </div>
        </form>
      </section>
    </main>
  );
}
