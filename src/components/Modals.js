'use client';

import { Button, Modal } from 'flowbite-react';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { deleteProduct } from '../redux/actions/productActions';

export function Modals({openModal, onCloseModal, data, action}) {
  console.log(onCloseModal)
  console.log('open modal', openModal)
  return (
    <>
      <Modal show={action == "delete" ? false: openModal} onClose={onCloseModal}>
        <Modal.Header>{data.title}</Modal.Header>
        <Modal.Body>
          <div className="space-y-6">
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              {data.description}
            </p>
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              {data.price}
            </p>
            <div className='grid grid-cols-3 gap-2'>
                {
                    data.images && data.images.map((image) => <img 
                        src={image} alt={data.title} />)
                }
            </div>
            
          </div>
        </Modal.Body>
        
      </Modal>
    </>
  );
}


export function ModalDelete({openModal, onCloseModal, id, action}) {
  const dispatch = useDispatch()

  console.log("onCloseModal", onCloseModal)
  console.log('open modal', openModal)
  return (
    <>
      <Modal show={action == "delete" ? openModal : false} onClose={onCloseModal}>
        <Modal.Header>Are you sure to delete?</Modal.Header>
        <Modal.Body>
          <div className="space-y-6">
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              If you click on Ok, product id {id} will be delete forever
            </p>
              
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => {
            dispatch(deleteProduct(id))}
          }>Ok</Button>
          <Button color="gray" onClose={onCloseModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
