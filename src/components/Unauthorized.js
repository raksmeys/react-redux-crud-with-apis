import { Button } from "flowbite-react";
import { useNavigate } from "react-router-dom";

export function Unauthorized(){
    const navigate = useNavigate()
    return(
        <main className="container mx-auto h-screen flex items-center justify-center">
            <section class="bg-white dark:bg-gray-900">
                <div class="mx-auto">
                    <div class="mx-auto max-w-screen-sm text-center">
                        <h1 class="mb-4 text-7xl tracking-tight font-extrabold lg:text-9xl text-primary-600 dark:text-primary-500">Unauthorized</h1>
                        <p class="mb-4 text-3xl tracking-tight font-bold text-gray-900 md:text-4xl dark:text-white text-center">You are not allowed to visit this page.</p>
                        <p class="mb-4 text-lg font-light text-gray-500 dark:text-gray-400">We are already working to solve the problem. </p>
                        <Button
                                className="block mx-auto"
                                onClick={() => navigate("/")} 
                                label="2">Go to Home</Button>
                    </div>  
                    </div>
            </section>
        </main>
    )
}