import { Outlet } from "react-router-dom";
import Footer from "./Footer";
import Navbar, { AdminNavbar } from "./Navbar";
import Aos from 'aos'
import 'aos/dist/aos.css'
import { useEffect } from "react";

export default function MainLayout() {

  useEffect(() => {
    Aos.init({
      duration: 3000
    })
  }, [])
  document.body.classList.add('dark:bg-gray-900')

  return (
    <>
        <Navbar />
            <Outlet />
        <Footer />
    </>
  )
}

export function AdminLayout(){
  document.body.classList.add('dark:bg-gray-900')
  return(
    <>
        {/* will put admin navbar */}
        <AdminNavbar />
        <Outlet />
    </>
  )
}
