import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

import { Autoplay, Pagination, Navigation } from 'swiper/modules';

import { Card } from './Card';
export function MeySwiper({perView, data}){
    return(
        <>
            <h2 className='text-4xl font-extrabold dark:text-white mb-5'>Popular Products</h2> 
            <section className='container mx-auto'>
                <Swiper
                    spaceBetween={50}
                    slidesPerView={perView}
                    autoplay={{
                        delay: 3000,
                        disableOnInteraction: false
                    }}
                    navigation={true}
                    modules={[Autoplay, Pagination, Navigation]}
                    onSlideChange={() => console.log('slide change')}
                    onSwiper={(swiper) => console.log(swiper)}
                >
                {
                    data.map(d => 
                        <SwiperSlide>
                            <Card product={d} />
                        </SwiperSlide>    
                    )
                }     
                </Swiper>
            </section>
        </>
    )
}