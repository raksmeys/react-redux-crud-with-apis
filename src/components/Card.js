import { Link } from "react-router-dom";

export function Card({product}){
    return(
        <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                <img 
                    class="rounded-t-lg" 
                    src={product.images[0]} 
                    alt={product.title} />
            <div class="p-5">
                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white line-clamp-1">
                        <Link to={"/products/"+ product.id}>{product.title}</Link>
                    </h5>
                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 line-clamp-2">{product.description}</p>
                {/* <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                        {product && product.category.name}
                </h5> */}
            </div>
        </div>
    )
}