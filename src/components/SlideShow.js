'use client';

import { Carousel } from 'flowbite-react';

export function SlideShow() {
  return (
    <section className="h-100 sm:h-64 xl:h-[48rem] 2xl:h-96">
      <Carousel slideInterval={3000}>
          <img
            src="https://api.istad.co/media/image/ca4fca81-5460-40c8-9d6b-e9270cd2ecae.png" alt="..." />
          <img 
            src="https://api.istad.co/media/image/0b7ddba0-021c-4dc3-ad73-6fe8bea44167.png" alt="..." />
        <img  
          src="https://api.istad.co/media/image/8665a243-b962-4a59-b51a-f31a3704b701.png" alt="..." />
        <img 
          src="https://api.istad.co/media/image/e4a4d369-72c3-441c-9df1-23cc6e2ce3f7.jpg" alt="..." />
        <img 
          src="https://api.istad.co/media/image/77da9050-7ff8-40af-a317-11d097cfacb7.png" alt="..." />
      </Carousel>
    </section>
  );
}
