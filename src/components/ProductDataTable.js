import { useEffect, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';

import { ModalDelete, Modals } from "./Modals";
import DataTable from "react-data-table-component";
import { searchProducts } from "../redux/actions/productActions";
import { Button, TextInput } from "flowbite-react";

export default function ProductDataTable(){

    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [query, setQuery] = useState("")
    const [id, SetId] = useState(0)

    // modal state
  const [openModal, setOpenModal] = useState(false);
  const [status, setStatus] = useState("")
  const [product, setProduct] = useState({})

    // received from global state
    const {products} = useSelector(state => state.productR)
    const { isLoggedIn } = useSelector(state => state.authR);

    useEffect(() => {
        dispatch(searchProducts(query))
    }, [query])

    const columns = [
        {
            name: 'Title',
            selector: row => row.title,
            sortable: true
        },
        {
            name: 'Category',
            selector: row => row.category.name,
            sortable: true
        },
        {
            name: 'Price',
            selector: row => `${row.price} $`,
            sortable: true
        },
        {
            name: 'Thumbnail',
            selector: row => <img
                className="w-[100px] m-2"
                src={row.images[0]} />,
        },
        {
            name: "Action",
            selector: row => <div className="grid grid-cols-3 gap-2">
                <Button onClick={() => {
                        setOpenModal(true);
                        setStatus("view")
                        setProduct(row);
                    }}
                    color="success"
                >
                    View
                </Button>
                <Button 
                    onClick={() => {
                        navigate("/product/edit", {
                            state: row
                        })
                    }}
                    color="success">Edit</Button>
                    <Button onClick={() => {
                        setOpenModal(true);
                        SetId(row.id)
                        setStatus("delete")
                    }}
                        color="failure"
                    >
                    Delete
                    </Button>
            </div>
        }
    ];
    
    return isLoggedIn ? (
        <>
            
            <Modals
            openModal={openModal}
            onCloseModal={() => setOpenModal(false)}
            data={product}
            action={status}
            />

            <ModalDelete 
                openModal={openModal}
                onCloseModal={() => setOpenModal(false)}
                id={id}
                action={status}
            />

            <DataTable 
                data={products}
                columns={columns}
                pagination
                subHeader
                subHeaderComponent={
                    <div className="max-w-md w-full">
                        <TextInput
                            onChange={(e) => {
                                setQuery(e.target.value)
                            }}
                            type="text" 
                            placeholder="Searching..." required />
                    </div>
                }
            />
        </>
            
        ): (
            <Navigate to={"/unauthorized"} />
        )
}
