import React, { useEffect, useState } from 'react'
import { fetchProducts } from '../services/productActions'
import Loading from '../components/Loading'
import { Card } from '../components/Card'
import { SlideShow } from '../components/SlideShow'
import { Helmet } from 'react-helmet'

export default function Products() {
    const [isLoading, setIsLoading] = useState(true)
    const [products, setProducts] = useState([
        {
          title: "Vision Pro",
          images: ["https://eduport.webestica.com/assets/images/courses/4by3/04.jpg"],
          description: "Hehe",
          category: {
            name: "Electronics"
          }
        }
      ])
    
    useEffect(() => {
        fetchProducts(12)
        .then(response => {
            setProducts(response)
            setIsLoading(false)
        })
    }, [])
     
  return (
    <>
        <Helmet>
            <meta charSet="utf-8" />
            <title>CSTAD - Products</title>
            <link rel="canonical" href="https://istad.co/" />
            <meta name="title" content="Web Design Class" />
            <meta name="description" content="Web Design course is designed for students to get started with Design Website Concepts. This course will focus on basic and advanced layout of website plus responsive design including UI/UX Design Concept. Moreover, CMS &amp; Dynamic content with hosting application to access in public." />
            <meta name="thumbnail" content="https://api.istad.co/media/image/3a3d2bf2-670b-4f2f-a72a-b9b8ba3a0f38.png" />
            <meta property="og:title" content="Available Courses" />
            <meta property="og:description" content="There are many courses of ISTAD that you can see" />
            <meta property="og:image" content="https://www.istad.co/resources/img/istad-thumbnail.png" />
        </Helmet>
        <main className='mt-20 container mx-auto mb-10'>
            <SlideShow />
            <section className='grid grid-cols-1 md:grid-cols-4 gap-4 mt-10'>    
                {
                isLoading ? <Loading /> : products.map(pro => <Card product={pro}/>)
                }
            </section>
        </main>
    </>
  )
}
