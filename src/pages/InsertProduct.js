import React from 'react'
import FormProduct from '../components/FormProduct'

export default function InsertProduct() {
  return (
    <main className='container mx-auto mt-[120px]'>
        <section>
            <FormProduct />
        </section>
    </main>
  )
}
