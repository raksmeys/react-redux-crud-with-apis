import { initFlowbite } from "flowbite"
import { useEffect } from "react"
import ProductDataTable from "../components/ProductDataTable"
import { useNavigate } from "react-router-dom"

export default function Dashboard(){
    const navigate = useNavigate()
    useEffect(() => {
        initFlowbite()
    },[])
    return(
        <>
            {/* <!-- Left Sidebar --> */}
            

            <main className="p-4 md:ml-64 h-auto pt-20">
                <button 
                    onClick={() => navigate("/product/insert")}
                    type="button" className="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2">Create New Product</button>
                {/* call data table */}
                <ProductDataTable />
            </main>
        </>
    )
}