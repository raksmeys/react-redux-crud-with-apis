import React from 'react'
import { Helmet } from 'react-helmet'
import { Link } from 'react-router-dom'

export default function Contact() {
  return (
    <>
      <Helmet>
            <meta charSet="utf-8" />
            <title>CSTAD - Contact</title>
            <link rel="canonical" href="https://istad.co/" />
            <meta name="title" content="Web Design" />
            <meta name="description" content="Web Design course is designed for students to get started with Design Website Concepts. This course will focus on basic and advanced layout of website plus responsive design including UI/UX Design Concept. Moreover, CMS &amp; Dynamic content with hosting application to access in public." />
            <meta name="thumbnail" content="https://api.istad.co/media/image/3a3d2bf2-670b-4f2f-a72a-b9b8ba3a0f38.png" />
            <meta property="og:title" content="Available Courses" />
            <meta property="og:description" content="There are many courses of ISTAD that you can see" />
            <meta property="og:image" content="https://www.istad.co/resources/img/istad-thumbnail.png" />
        </Helmet>
    <main>
      <section className="bg-white dark:bg-gray-900 mt-10">
        <div className="gap-8 items-center py-8 px-4 mx-auto max-w-screen-xl xl:gap-16 md:grid md:grid-cols-2 sm:py-16 lg:px-6">
            <img className="w-full dark:hidden" src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/cta/cta-dashboard-mockup.svg" alt="dashboard image" />
            <img className="w-full hidden dark:block" src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/cta/cta-dashboard-mockup-dark.svg" alt="dashboard image" />
            <div className="mt-4 md:mt-0">
                <h2 className="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">This className also teaching on customize admin dashboard</h2>
                <p className="mb-6 font-light text-gray-500 md:text-lg dark:text-gray-400">CSTAD helps you students working on user front site as well as admin dashbaord work in one course <blockquote>Web Design </blockquote> </p>
                <Link to="/dashboard" className="inline-flex items-center text-white bg-primary-700 hover:bg-primary-800 focus:ring-4 focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:focus:ring-primary-900">
                    Get started
                    <svg className="ml-2 -mr-1 w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                </Link>
            </div>
        </div>
      </section>
    </main>
    
    </>
  )
}
